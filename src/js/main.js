/* Your JS here. */
var y = window.scrollY;
var t1 = document.getElementById("taskbar-li1");
var t2 = document.getElementById("taskbar-li2");
var t3 = document.getElementById("taskbar-li3");
var navbar = document.getElementById("navbar");
var carousel = document.getElementById("carousel");
var slides = document.getElementById("slides");
var modal = document.getElementsByClassName("modal")[0];
var grid = document.getElementsByClassName("grid")[0];
var videos = modal.children;
console.log('Hello World!');

t1.onclick = () => window.scrollTo(0,0); 
t2.onclick = () => window.scrollTo(0, carousel.offsetTop);
t3.onclick = () => window.scrollTo(0, grid.offsetTop);




window.addEventListener("scroll", () => {
    var fsize;
    if (window.scrollY < carousel.offsetTop) {
        t1.style.backgroundColor = "grey";
        t2.style.backgroundColor = "transparent";
        t3.style.backgroundColor = "transparent";
        
    }
    else if (window.scrollY < grid.offsetTop) {
        t2.style.backgroundColor = "grey";
        t1.style.backgroundColor = "transparent";
        t3.style.backgroundColor = "transparent";
    }
    else {
        t3.style.backgroundColor = "grey";
        t1.style.backgroundColor = "transparent";
        t2.style.backgroundColor = "transparent";
    }

    if (window.scrollY < 100) {
        let new_height = window.innerHeight / 5 - (window.innerHeight / 1000 * window.scrollY);
        navbar.style.height = new_height + "px";
        fsize = 3 - window.scrollY / 100;
        fsize = fsize + "em";
    }
    else {
        fsize = "2em";
        navbar.style.height = "10%";
    }

    let children = navbar.children;
    for (let i = 0; i < children.length; i++) {
        children[i].style.fontSize = fsize;
    } 
});


var curr_slide = 0;

function moveSlides(n) {
    let prev_slide = curr_slide;
    curr_slide = (curr_slide + n) % 3;
    if (curr_slide < 0) {
        curr_slide += 3;
    }   

    let slides_list = slides.children;
    let prev_elem = slides_list[prev_slide];
    let curr_elem = slides_list[curr_slide];

    let i = 1;
    var timer = setInterval(function () {
        console.log(curr_elem.style.opacity);
        if (i <= 0.05) {
            clearInterval(timer);
        }
        i = i - .05;
        prev_elem.style.opacity = i;
        curr_elem.style.opacity = 1 - i;
    }, 50);
}

window.moveSlides = moveSlides;

var curr_vid = 0;

function openModal(n) {
    curr_vid = n;
    modal.style.display = "block";
    videos[n].style.display = "flex";
    videos[4].style.display = "flex";
}

function closeModal() {
    modal.style.display = "none";
    videos[curr_vid].style.display = "none";
    videos[curr_vid].pause();
    videos[4].style.display = "none";
}

window.openModal = openModal;
window.closeModal = closeModal;